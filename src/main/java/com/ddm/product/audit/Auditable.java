package com.ddm.product.audit;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<U> {
	
	@CreatedBy
	@Column(name = "user_id")
	@NotNull
	private U userId;
	
    @CreatedDate
    @Column(name = "created_ts")
    @NotNull
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_updated_ts")
    @NotNull
    private LocalDateTime lastModifiedDate;
}

