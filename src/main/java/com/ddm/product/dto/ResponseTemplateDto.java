package com.ddm.product.dto;

import com.ddm.product.enums.ResponseType;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ResponseTemplateDto<T> {
	
	private ResponseType responseType;
	private T result;

}
