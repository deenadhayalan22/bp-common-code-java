package com.ddm.product.dto;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class UserInfo {
	
	protected UUID userId;
	protected String email;
	protected String password;
    
    public UserInfo(UserInfo userInfo) {
    	this.userId=userInfo.userId;
    	this.email=userInfo.email;
    	this.password=userInfo.password;
    }

	public UserInfo() {
		
	}

}
