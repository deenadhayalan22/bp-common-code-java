package com.ddm.product.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.ddm.product.dto.ApiError;
import com.ddm.product.dto.ResponseTemplateDto;
import com.ddm.product.enums.ResponseType;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException {
        log.error("Responding with unauthorized error. Message - {}", e.getMessage());
        
        final ApiError apiError = 
      	      new ApiError(HttpStatus.UNAUTHORIZED, "Resource is not allowed without authorization");
        
      	final ResponseTemplateDto<ApiError> response = new ResponseTemplateDto<>(ResponseType.FAILURE,
                      apiError);
      	ObjectMapper objectMapper = new ObjectMapper();
        String resBody = objectMapper.writeValueAsString(response);
      	    
        httpServletResponse.setContentType("application/json");
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		
        PrintWriter printWriter = httpServletResponse.getWriter();
        printWriter.print(resBody);
        printWriter.flush();
        printWriter.close();
    }
}
