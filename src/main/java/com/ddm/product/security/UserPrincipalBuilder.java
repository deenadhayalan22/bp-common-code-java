package com.ddm.product.security;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.ddm.product.dto.UserInfo;
import com.ddm.product.dto.UserPrincipal;

public class UserPrincipalBuilder {
	
	public static UserPrincipal create(UserInfo user) {
        List<GrantedAuthority> authorities = Collections.
                singletonList(new SimpleGrantedAuthority("ROLE_USER"));

        return new UserPrincipal(
        		user,
                authorities
        );
    }
	
	public static UserPrincipal create(UserInfo user, Map<String, Object> attributes) {
        UserPrincipal userPrincipal = UserPrincipalBuilder.create(user);
        userPrincipal.setAttributes(attributes);
        return userPrincipal;
    }

}
