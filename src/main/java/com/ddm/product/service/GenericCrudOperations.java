package com.ddm.product.service;

import java.util.List;

public interface GenericCrudOperations<T> {
	
	T addOneRecord(T t);
	
	List<T> addAllRecords(List<T> t);
	
	void deleteOneRecord(Long id);
	
	List<T> getAllRecords();
	
	T getOneRecord(Long id);
}
