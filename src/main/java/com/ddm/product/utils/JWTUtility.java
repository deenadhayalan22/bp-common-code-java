package com.ddm.product.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.ddm.product.dto.UserInfo;
import com.ddm.product.dto.UserPrincipal;
import com.ddm.product.security.UserPrincipalBuilder;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class JWTUtility {

	@Value("${jwt.tokenExpirationMsec:1}")
    private Long tokenExpirationMsec;
    
	@Value("${jwt.tokenSecret:ddm}")
    private String tokenSecret;

    public String createToken(Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + tokenExpirationMsec);
        
        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("userId", userPrincipal.getUserId());
        userInfo.put("email", userPrincipal.getEmail());
        userInfo.put("password", userPrincipal.getPassword());

        return Jwts.builder()
                .setSubject(userPrincipal.getUserId().toString())
                .setClaims(userInfo)
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, tokenSecret)
                .compact();
    }
    
    public UserPrincipal getPrincipalFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(tokenSecret)
                .parseClaimsJws(token)
                .getBody();
        
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(UUID.fromString(claims.get("userId").toString()));
        userInfo.setEmail(claims.get("email").toString());
        userInfo.setPassword(token);

        return UserPrincipalBuilder.create(userInfo);
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(tokenSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
        	log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
        	log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
        	log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
        	log.error("JWT claims string is empty.");
        }
        return false;
    }
}
