package com.ddm.product.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ddm.product.dto.ResponseTemplateDto;
import com.ddm.product.enums.ResponseType;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ResponseBuilder {
	
	public <T> ResponseEntity<ResponseTemplateDto<T>> created(T result) {
		ResponseTemplateDto<T> response = new ResponseTemplateDto<>(ResponseType.SUCCESS, result);
		return new ResponseEntity<>(response, HttpStatus.CREATED);		
	}
	
	public <T> ResponseEntity<ResponseTemplateDto<T>> success(T result) {
		ResponseTemplateDto<T> response = new ResponseTemplateDto<>(ResponseType.SUCCESS, result);
		return new ResponseEntity<>(response, HttpStatus.OK);		
	}

}
