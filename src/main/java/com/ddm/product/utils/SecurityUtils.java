package com.ddm.product.utils;

import java.util.UUID;

import org.springframework.security.core.context.SecurityContextHolder;

import com.ddm.product.dto.UserPrincipal;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SecurityUtils {
	
	public UUID getUserId() {
		UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return principal.getUserId();
	}

}
